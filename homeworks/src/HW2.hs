-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown contact = case contact of
    On -> True
    Off -> True
   Unknown -> False

deriving Eq
data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval term = case term of
   (Mult term1 term2) -> eval term1 * eval term2
   (Add term1 term2) -> eval term1 + eval term2
   (Sub term1 term2) -> eval term1 - eval term2
   (Const int) -> int

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Termimplify term = simplifyWithMemory term (Const 0)

simplifyWithMemory :: Term -> Term -> Term
simplifyWithMemory term prevTerm
    | term == prevTerm = term
    | otherwise = case term of
      Mult a (Add b1 b2) -> Add (simplify (Mult a b1)) (simplify (Mult a b2))
      Mult (Add b1 b2) a -> Add (simplify (Mult a b1)) (simplify (Mult a b2))

       Mult a (Sub b1 b2) -> simplify $ Sub (simplify (Mult a b1)) (simplify (Mult a b2))
       Mult (Sub b1 b2) a -> simplify $ Sub (simplify (Mult a b1)) (simplify (Mult a b2))

       Mult a (Mult b1 b2) -> simplifyWithMemory (Mult (simplify a) (simplify (Mult b1 b2))) term 
       Mult (Mult b1 b2) a -> simplifyWithMemory (Mult (simplify a) (simplify (Mult b1 b2))) term 

        Sub a (Add b1 b2) -> simplify $ Sub (simplify (Sub a b1)) (simplify b2)
        Sub a (Sub b1 b2) -> Add (simplify (Sub a b1)) (simplify b2)
        Sub a (Const i) -> Sub (simplify a) (Const i)
        Sub a (Mult b1 b2) -> simplifyWithMemory (Sub (simplify a) (simplify (Mult b1 b2))) term 

        Add a b -> Add (simplify a) (simplify b)

        a -> a

